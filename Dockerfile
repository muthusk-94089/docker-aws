FROM alpine:3.3

MAINTAINER Jeremy Pruitt <jpruitt@juniper.net>

ENV ENVCONSUL_VERSION 0.6.1

WORKDIR /tmp

RUN apk add --no-cache \
        bash \
        bash-completion \
        curl \
        git \
        groff \
        jq \
        less \
        py-pip \
        python \
        unzip \
        wget \
 && pip install --upgrade \
        pip \
        awscli \
        python-dateutil \
        colorama==0.3.7 \
        awsebcli \
 && ln -s /usr/bin/aws_bash_completer /etc/profile.d/aws_bash_completer.sh \
 && rm -rf /tmp/*

RUN apk update
RUN apk add ca-certificates
RUN apk add wget tar
RUN wget -q --no-check-certificate -O /tmp/envconsul.zip https://releases.hashicorp.com/envconsul/${ENVCONSUL_VERSION}/envconsul_${ENVCONSUL_VERSION}_linux_amd64.zip
RUN unzip -d /tmp /tmp/envconsul.zip
RUN rm /tmp/envconsul.zip
RUN mv /tmp/envconsul /usr/bin
RUN apk del wget tar
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* *.gz
RUN rm -rf /var/cache/apk/*
